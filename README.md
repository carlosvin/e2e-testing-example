
You can find the tests source code under [cypress/integrations folder](https://gitlab.com/carlosvin/e2e-testing-example/-/tree/master/cypress/integration).

We have some tests [checking the business logic](https://gitlab.com/carlosvin/e2e-testing-example/-/blob/master/cypress/integration/test_codes.spec.js) and others [checking the web accessibility](https://gitlab.com/carlosvin/e2e-testing-example/-/blob/master/cypress/integration/test_accessibility.spec.js).

# Running the tests

Clone this repository and run all the following commands from project root folder.

```bash
git clone https://gitlab.com/carlosvin/e2e-testing-example
cd e2e-testing-example
```

## In a Docker container

### Headless (Recommended)
```bash
docker run -it -v $PWD:/e2e -w /e2e carlosvin/cypress-axe-plugin:0.0.1
```
Test execution will print a report on console and it will generate videos and screenshots of the test execution in `cypress/screenshots` and `cypress/videos` folders.

#### Copy screenshots/videos from container to local host
```
sudo docker cp <container_id>:/cypress/screenshots/ $PWD/screenshots
```

### Interactive mode
> You need to forward the XVFB messages from Cypress out of the Docker container into an X11 server running on the host machine[...] [Cypress documentation](https://www.cypress.io/blog/2019/05/02/run-cypress-with-a-single-docker-command#Interactive-mode)

## In your local host

### Install required packages
```
npm install
```
or
```
yarn i
```
### Open Cypress test runner
```
npm run cypress:open
```
or
```
yarn cypress:open
```

# Why Cypress?
The problem: We have to test if a web page is working as expected, so first thing that comes to my mind is Selenium. It is a mature safe choice which supports cross browser testing, although it is not quite developer friendly. 

But there are other options to consider, like [Cypress](https://www.cypress.io/). It is quite easy to start with, but it works only with [Chrome family browsers and FF (in beta stage)](https://cypress.io/blog/2020/02/06/introducing-firefox-and-edge-support-in-cypress-4-0/?utm_campaign=Docs%20Banner&utm_medium=blog%20post&utm_source=blog&utm_content=text%20link). [IE and Safari are unsupported](https://docs.cypress.io/guides/guides/launching-browsers.html#Unsupported-Browsers), although the support for both is under consideration: https://github.com/cypress-io/cypress/issues/6422 and https://github.com/cypress-io/cypress/issues/6423.

Considering that IE + Safari are less than 12% of the market, I would go for simplest solution ([Cypress](https://www.cypress.io/)) with easier learning curve for our developers.

FROM cypress/included:4.0.1

COPY cypress cypress
COPY cypress.json .
COPY package.json .
COPY yarn.lock .
RUN yarn install
ENTRYPOINT ["yarn", "cypress:run"]

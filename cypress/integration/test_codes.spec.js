

const BASE_URL = 'https://codes.dev.rebellionpay.com'

// If the code is empty:
const RESP_NO_CODE = {
    error: "No code",
    result: null
}

// Code is not in the list:
const RESP_NOT_IN_LIST = {
    error: "Incorrect code",
    result: null
}

function respInList(code) {
    return {
        result: `The code: ${code}`,
        error: null
    }
}

function assertResponse(response) {
    cy.url().should('include', '/result.html')
    cy.get('body').invoke('text').should((content) => {
        expect(JSON.parse(content)).to.deep.equal(response)
    })
}

function typeCode(code) {
    cy.get('#code').type(code).should('have.value', code)
}

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test when there is an unhandled exception
    console.warn(err, runnable)
    return false
})

describe('Valid codes', function () {
    const VALID_CODES = [
        "REB001",
        "REB002",
        "REB003",
        "REB004",
        "REB005",
        "REB006",
        "REB007"
    ];

    VALID_CODES.forEach((code) => {
        it(`submits a valid code "${code}"`, () => {
            cy.visit(BASE_URL)
            typeCode(code)
            cy.get('#rebelform').submit()
            assertResponse(respInList(code))
        })
    })

    VALID_CODES.forEach((code) => {
        it(`submits by clicking on form button a valid code "${code}"`, () => {
            cy.visit(BASE_URL)
            typeCode(code)
            cy.get('#rebelform > input[type=button]').click()
            assertResponse(respInList(code))
        })
    })
})

describe('Invalid codes', function () {
    const INVALID_CODES = [
        "REB008",
        "REB000",
        "REB0001",
        "ASDF",
        "looooooooooooooooooooong1",
        "reb001",
        "reb007",
        " "
    ];

    it(`submits an empty code`, () => {
        cy.visit(BASE_URL)
        cy.get('#rebelform').submit()
        assertResponse(RESP_NO_CODE)
    })

    INVALID_CODES.forEach((code) => {
        it(`submits an invalid code: "${code}"`, () => {
            cy.visit(BASE_URL)
            typeCode(code)
            cy.get('#rebelform').submit()
            assertResponse(RESP_NOT_IN_LIST)
        })
    })
})
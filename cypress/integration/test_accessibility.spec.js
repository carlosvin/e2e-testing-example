

const BASE_URL = 'https://codes.dev.rebellionpay.com'

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test when there is an unhandled exception
    console.warn(err, runnable)
    return false
})

describe('Accessibility', function () {
    it(`loads page and check A11y`, () => {
        cy.visit(BASE_URL)
        cy.injectAxe()
        cy.checkA11y()
    })
})
